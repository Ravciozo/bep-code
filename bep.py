"""
Decided to put here all the BEP functions as well, to then have a clear script of their use
"""

#Installing dependencies:
import spacy
from spacypdfreader import pdf_reader
import os
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
import re
import itertools
import pandas as pd
import selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
#from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
import string
import urllib, urllib.request
import csv
from nltk.stem import WordNetLemmatizer

wnl = WordNetLemmatizer()

brown_ic = wordnet_ic.ic('ic-brown.dat')

nlp = spacy.load('en_core_web_sm')

download_dir = 'C:\\Users\\user\\Desktop\\TUe\\BEP\\downloads'
standard_dir = 'C:\\Users\\user\\Desktop\\TUe\\BEP'

month_header = '/html/body/div/section/div[2]/div[1]/dl/dt[3]'
month_name = '/html/body/div/section/div[2]/div[1]/dl/dd[3]'
year_header = '/html/body/div/section/div[2]/div[1]/dl/dt[4]'
year_name = '/html/body/div/section/div[2]/div[1]/dl/dd[4]'

def version() -> str:
    '''Returns the current version of the library'''
    
    return("0.1")


#arxiv_revived notebook:
def data_stripper(text : str):
    '''Yes, that is a really bad name for a function
    Decodes the data inside
    Saves only the relevant info from the API output.'''
    
    #Throwing away everything that comes before the actual entry:
    begin = text.find('<entry>') + 7 #There are 7 characters in <entry>
    end = text.find('</entry>')
    text = text[begin:end]
    
    #Taking the publication date:
    begin = text.find('<published>') + 11
    end = text[begin:].find('</published>') + begin - 1
    publication_date = text[begin:end]
    
    #Taking the title:
    begin = text.find("<title>") + 7 #There are 7 characters in <title>
    end = text[begin:].find("</title") + begin #Adding begin since the numeration will restart once we take the slice
    title = text[begin:end]
    enter = title.find('\n')
    if (enter != -1):
        title = title[:enter] + title[enter+2:]
    
    #Taking the author (for layout purposes mostly):
    begin = text.find('<name>') + 6
    end = text[begin:].find('</name>') + begin
    author = text[begin:end]
    enter = author.find('\n')
    if (enter != -1):
        author = author[:enter] + author[enter+2:]
    
    #Here we already shall get rid of the first part of the data:
    #print("end = ", end)
    text = text[end:]
    
    #Taking the metadata link:
    begin = text.find('<link href=') + 12
    end = text[begin:].find('rel') + begin - 2
    link = text[begin:end]
                            
    #Taking the PDF link:
    begin = text.find('<link title="pdf"') + 24
    end = text[begin:].find('rel') + begin - 2
    pdf_link = text[begin:end]
    
    #Finding the version:
    version = link[-2:] #Takes only the last 2 characters - not enough for 10+ versions!
    
    return(title, author, publication_date, link, pdf_link, version)

def data_collector(query : str, max_iterations : int, addition : int, verbose = False):
    '''Returns the title, author, link to metadata, link to pdf and the paper version
    Addition of 20 and max iteration of 4 makes the function check the 20, 21, 22 and 23rd position'''
    
    return_array = [0] * max_iterations
    for i in range(0, max_iterations):
        #Sleeping:
        time.sleep(5)
        if (verbose):
            print("leaving sleep time {}".format(i))
        
        #Formatting the url query:
        url = 'http://export.arxiv.org/api/query?search_query=all:{}&start={}&max_results={}'.format(query, i+addition, i+1+addition)
        
        #Sometimes the arXiv will not respond, so we would like to omit those queries:
        try:
            #Decoding the response:
            data = urllib.request.urlopen(url)
            decoded = data.read().decode('utf-8')
            time.sleep(1)
            
            #Stripping the response of filler stuff and saving the output to an array:
            return_array[i] = data_stripper(decoded)
            for element in return_array[i]:
                if (element == None):
                    print("Data stripping failed at i =", i+addition)
        #And if arXiv did not respond, know for which papers did it not respond:
        except:
            print("decoding failed at i =", i+addition)
        
    return(return_array)

def saving_to_a_file(output, file_name : str, mode = 'w', 
                     header = ['title', 'author', 'publication_date', 'metadata_link', 'pdf_link', 'version']) -> None:
    '''use the output of data_collector()
    output_csv.csv is generally the file
    pass mode = "w" for erasing the content of the file and overwriting it
    pass mode = "a" for appending the content of the file'''
    
    i = 0 #iteration counter to locate issues:
    
    with open(file_name, mode) as file:
        #Putting semicolon as a delimiter, as otherwise the commas in paper's title split the csv cells xD
        writer = csv.writer(file, delimiter = ';')
    
        #Writing the header to the csv:
        if (mode == "w"):
            writer.writerow(header)
    
        #Writing the rows to the csv:
        for row in output:
            if (row != [0]): #Only dealing with the non-broken cells:
                try:
                    writer.writerow(row)
                except:
                    print(row, i)
            i = i + 1


#ACL web scraper notebook:
def create_driver(download_dir = download_dir):
    '''
    Set it in stone for downloading into the download_dir. Possibly change in future
    '''
    
    os.chdir(download_dir)

    #Options to save PDFs automatically:
    options = Options()
    options.add_experimental_option('prefs',  {
        "download.default_directory": download_dir,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "plugins.always_open_pdf_externally": True
        }
    )
    
    #Setting the scraper up and opening ACL
    driver = webdriver.Chrome(executable_path=r"C:\Users\user\Desktop\TuE\BEP\chromedriver.exe", options=options)
    driver.get("https://aclanthology.org/")

    #Some delay to let the page load:
    time.sleep(6) #5-6 should be easily enough
    
    main_tab = driver.current_window_handle
    
    return(driver, main_tab)

def switch_tabs(driver, main_tab, verbose = False):
    '''returns the driver'''
    
    if (driver.current_window_handle == main_tab):
        if (verbose):
            print('entered the if in switch_tabs')
        #get first child window:
        chwd = driver.window_handles

    #Looping over different tabs to switch to a first different one:
    for window in chwd:
        #print(window)
        if (window != driver.current_window_handle):
            if (verbose):
                print('switched the tabs within the function of switch_tabs')
            driver.switch_to.window(window) #This is how you switch tabs in Selenium
            break
            
    return(driver)

def check_n_first_results(title, driver, n = 3, verbose = False):
    '''search successful: return the driver
    search unsuccessful: return None
    Search is successful if the title of the result is there embedded within the title sought
    The click on the proper link is done within this function
    verbose mode: printing specific steps, for debugging purposes'''
    
    title = title.lower()
    
    for i in range(0, n):
        xpath = '//*[@id="___gcse_0"]/div/div/div/div[5]/div[2]/div[1]/div/div[1]/div[{}]/div[1]/div[1]/div/a'.format(i+1)
        try: #If any result appeared on the page:
            result = driver.find_element(By.XPATH, xpath)
        except NoSuchElementException: #If there literally aren't any results to the query:
            break
        text = result.text
        if (verbose): #Only printing if you specifically ask for the prints to be there
            print("text before =", text)
        ACL_start = text.find(' - ACL')
        if (ACL_start != -1):
            text = text[:ACL_start]
        else:
            text = text[:-4]
        if (verbose): #Only printing if you specifically ask for the prints to be there
            print("text after =", text)
        text = ''.join([i.lower() for i in text if i not in string.punctuation])
        if (text in title):
            if (verbose):
                print("Cliking the {}. result".format(i))
            result.click()
            return(driver)
    
    return(None)

def download_paper(title : str, driver, main_tab, verbose = False, save_as_title = True) -> None:
    '''
    TO DO: extract the date data from the site
    Beware: if the paper was not found, there is no way to know that!
    save_as_title puts the title as the file name
    save_as_title = False, sets it to 00_ACL
    '''

    title = ''.join([i for i in title if i not in string.punctuation])
    
    search_bar = driver.find_element(By.ID, 'acl-search-box')
    search_bar.click()
    
    search_bar.send_keys(Keys.CONTROL, 'a')
    search_bar.send_keys(Keys.BACKSPACE)

    search_bar.send_keys(title)

    search_bar.send_keys(Keys.ENTER)

    time.sleep(4) #Give the site some time to process your query
    
    if (verbose): #Only printing if you specifically ask for the prints to be there
        print("output to be determined")
    check_n_output = check_n_first_results(title, driver, n = 5)
    if (verbose): #Only printing if you specifically ask for the prints to be there
        print("output =", check_n_output)
    time.sleep(1)
    
    #If we have found any matching paper:
    if (check_n_output != None):
    
        #In case we need to switch the tab to click the PDF button:
        if (driver.current_window_handle == main_tab):
            if (verbose):
                print("Needing to switch the tabs")
            driver = switch_tabs(driver, main_tab, verbose=verbose)
        if (verbose):
            print("current tab = ", driver.title)
    
        try: #We were launched into a new tab, now we need to click the PDF button
            pdf_button = driver.find_element(By.XPATH, '/html/body/div/section/div[2]/div[2]/a[1]')
            pdf_button.click()
        except: #There was no new tab, so the file must be already downloaded
            try:
                pdf_button = driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div[2]/a[1]')
                pdf_button.click()
            except:
                print('no PDF button found')
            
        time.sleep(1)
    
    #If we are not already in the main tab:
    if (driver.current_window_handle != main_tab):
        driver.switch_to.window(main_tab)
    
    time.sleep(1) #If you're planning on terminating the download here, make the sleep longer

def download_arxiv_papers(file_name = 'output_csv.csv', using_v1 = True, 
                          download_dir='C:\\Users\\user\\Desktop\\TuE\\BEP\\downloads') -> None:
    '''
    ???
    '''
    
    #Setting the standard directory:
    os.chdir(download_dir)
    
    #Reading the csv file with arxiv papers' data and links:
    df = pd.read_csv('output_csv.csv')
    
    #Setting up the webdriver:
    driver, main_tab = create_driver()
    
    #For every pdf link in the csv:
    for element in df['pdf_link']:
        
        #Use the first uploaded version if the proper parameter was set:
        if (using_v1):
            element = element[:-2] + 'v1'
            
        #Download the element:
        #NOTICE THIS WILL DOWNLOAD TO THE 'DOWNLOADS' FOLDER
        driver.get(element)
    
    time.sleep(3) #Giving the site some time to download the papers
        
    return(None)
    
def save_pdf_link_and_date(driver, main_tab, title : str, month_name : str, year_name : str):
    '''
    Assuming the driver passed has only the main tab open
    Returns (pdf_link, month, year)...or nothing
    '''
    
    #Strips the title of punctuation:
    title = ''.join([i for i in title if i not in string.punctuation])
    title = title.lower()
    
    #Searches for the title:
    search_bar = driver.find_element(By.ID, 'acl-search-box')
    search_bar.click()
    search_bar.send_keys(Keys.CONTROL, 'a')
    search_bar.send_keys(Keys.BACKSPACE)
    search_bar.send_keys(title)
    try:
        search_bar.send_keys(Keys.ENTER)
    except StaleElementReferenceException:
        return(None)
    
    time.sleep(4) #Give the site some time to process your query
    
    #Searching if any paper's title matches the sought title:
    check_n_output = check_n_first_results(title, driver, n = 4)
    time.sleep(1)
    
    #If we found any matching paper:
    if (check_n_output != None):
    
        #In case we need to switch the tab to click the PDF button:
        if (driver.current_window_handle == main_tab):
            driver = switch_tabs(driver, main_tab, verbose=False)
    
        try: #We were launched into a new tab, now we need to click the PDF button
            pdf_button = driver.find_element(By.XPATH, '/html/body/div/section/div[2]/div[2]/a[1]')
            pdf_link = pdf_button.get_attribute("href")
            month = driver.find_element(By.XPATH, month_name).text
            year = driver.find_element(By.XPATH, year_name).text
        except: #There was no new tab, so the file must be already downloaded
            try:
                pdf_button = driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div[2]/a[1]')
                pdf_link = pdf_button.get_attribute("href")
                month = driver.find_element(By.XPATH, month_name).text
                year = driver.find_element(By.XPATH, year_name).text
            except:
                print('no PDF button found')
                
        #If we are not already in the main tab:
        if (driver.current_window_handle != main_tab):
            driver.close()
            driver.switch_to.window(main_tab)
        
        try:
            return(pdf_link, month, year)
        except UnboundLocalError:
            return(None)
 
 
#pdf_to_text notebook:
def cut_away_irrelevant(doc : str, verbose=False) -> str:
    '''
    :input:  the string document
    :output: the string document
    '''
    
    last_2_percent = len(doc) - len(doc)//50 #rounding to avoid floats here
    
    #Finding the indices of these sections. If a section was not found, -1 is returned
    headers = [section_header(doc, 8), section_header(doc, 9), section_header(doc, 10)]
    if(verbose):
        print(headers)
    headers.sort()
    
    #If all 3 headers were not found, just cut off the last 2% of the paper's volume:
    if (headers[2] == -1):
        return(doc[:last_2_percent])
    else:
        #One header found, 2 not found - returning the doc up to the last header:
        if (headers[1] == -1):
            doc = doc[:headers[2]]
        else:
            #Two headers found, 1 not found - returning the doc up to the middle header:
            if (headers[0] == -1):
                doc = doc[:headers[1]]
            else:
                #All headers found - returning the do up to the first header:
                doc = doc[:headers[0]]
    
    #Cut off the garbage before the Abstract. If Abstract not found, cut off the first 10 tokens
    if (section_header(doc, 0) != -1):
        doc = doc[section_header(doc, 0):]
    else:
        doc = doc[10:]
        
    return(doc)
    
def raise_to_minus_oneth(number : float):
    return(pow(number, -1))
   
def creating_idf_matrix(ents : list) -> pd.DataFrame:
    '''based on one simple document'''
    
    dic = {}
    for element in ents:
        if (element not in dic):
            dic[element] = 1.0
        else:
            dic[element] += 1

    df = pd.DataFrame.from_dict(data = dic, orient = 'index').transpose().apply(raise_to_minus_oneth)
    
    return(df)
   
def creating_idf_matrix(ents : list) -> pd.DataFrame:
    '''based on one simple document'''
    
    dic = {}
    for element in ents:
        if (element not in dic):
            dic[element] = 1.0
        else:
            dic[element] += 1

    df = pd.DataFrame.from_dict(data = dic, orient = 'index').transpose().apply(raise_to_minus_oneth)
    
    return(df)
    
def section_header(doc : str, nr_section : int):
    '''performs the optimal search for a header of the Introduction section.
    Takes into accounts multiple variations of how could such a section be given/named in text
    For example, searches for both 1. Introduction and 1) introduction and all in-between versions'''
    
    #If looking for Abstract:
    if (nr_section == 0):
        a = [['0', '1'], ['.', ')', ''], ['a', 'A'], ['bstract']]
    
    #If looking for Introduction:
    elif (nr_section == 1):
        a = [['1', '2', '3'], ['.', ')', ''], ['i', 'I'], ['ntroduction']]
        
    #If looking for Background:
    elif (nr_section == 2):
        a = [['1', '2', '3'], ['.', ')', ''], ['b', 'B'], ['ackground']]
    
    #If looking for Methods or Methodology:
    elif (nr_section == 3):    
        a = [['2', '3', '4'], ['.', ')', ''], ['m', 'M'], ['ethods', 'ethodology']]
        
    #If looking for Results:
    elif (nr_section == 4):
        a = [['2', '3', '4', '5'], ['.', ')', ''], ['r', 'R'], ['esults']]
        
    #If looking for Conclusion:
    elif (nr_section == 5):
        a = [['3', '4', '5'], ['.', ')', ''], ['c', 'C'], ['onclusion', 'onclusions']]
    
    #If looking for Discussion:
    elif (nr_section == 6):
        a = [['3', '4', '5', '6'], ['.', ')', ''], ['d', 'D'], ['iscussion']]
    
    #If looking for Future Work:
    elif (nr_section == 7):
        a = [['4', '5', '6', '7'], ['.', ')', ''], ['f', 'F'], ['uture Work', 'uture work']]
        
    #If looking for Acknowledgements:
    elif (nr_section == 8):
        a = [['4', '5', '6', '7', '8', ''], ['.', ')', ''], ['a', 'A'], ['cknowledgements']]
        
    #If looking for Bibliography:
    elif (nr_section == 9):
        a = [['4', '5', '6', '7', '8', '9', ''], ['.', ')', ''], ['b', 'B'], ['ibliography']]
        
    #If looking for References (which is the same as Bibliography, but this was speeds up the code):
    elif (nr_section == 10):
        a = [['4', '5', '6', '7', '8', '9', ''], ['.', ')', ''], ['r', 'R'], ['eferences']]
    
    #Actually creates the combinations from the given possibilities:
    a = list(itertools.product(*a))
    
    #Just concatenates the string:
    for quadruplet in a:
        sought = quadruplet[0] + quadruplet[1] + ' ' + quadruplet[2] + quadruplet[3] + "\n" #Not sure if this \n is fine
        
        #And this tries to locate such a string:
        placement = doc.find(sought)
        if (placement != -1):
            return(placement)
    
    #If the header was not found, return -1:
    return(-1)

def concatenating_dashes(ents : list) -> list:
    '''
    For any consecutive pair of words like:
    expres-
    -sion
    Merges them into one and deletes the next cell of the list
    '''
    
    i = 0
    while (i < len(ents)-1):
        if ((ents[i][-1] == '-') and (ents[i+1][0] == '-')):
            ents[i] = ents[i][:-1] + ents[i+1][1:]
            del ents[i+1]
            i -= 1
        i = i+1
        
    return(ents)
    
def erasing_non_words(ents : list) -> list:
    '''
    Tries to get rid of at least some of errors in the tokanization
    '''
    
    length = len(ents)
    symbols = ['[', ']', '@', '_', '∗', '(', ')'] + [str(_) for _ in range(10)]
    
    bool_list = [True] * length
    
    for i in range(0, length):
        for letter in ents[i]:
            if (letter in symbols):
                bool_list[i] = False
                break

    return([ents[i] for i in range(0, length) if bool_list[i]])
  
def tokenization_of_section(section_string : str):
    '''pass a single section of text as a parameter'''
    
    #Using the english thingy here:
    section_string = nlp(section_string)
    
    #Only interested in matching words that are 2-40 characters long
    #Reason? We HAVE TO match any "no"s!
    pattern = re.compile("\w{2,40}")
    ents = [e.text for e in section_string]
    ents = [ent for ent in ents if re.match(pattern, ent)]
    
    #Merges stuff like 'expres-', '-sion' together
    ents = concatenating_dashes(ents)
    
    #Erasing phrazes containing @, [, ], _, 0-9
    ents = erasing_non_words(ents)
    
    return(ents)
    
def text_set_creation(ents : list) -> list:
    '''For a list of tokens, POS-tag all of them
    Returns a list of tuples (word, POS-tag)'''

    #Sets storing NouNs, VerBs, ADJectives, ADVerbs and NUMerals respectively:
    NN, VB, ADJ, ADV, NUM = set(()), set(()), set(()), set(()), set(())

    #Iterating over all the tokens in this given section:
    for word in ents:
    
        #has to be converted to a list to tag (and then back again)
        tag = nltk.pos_tag([word])[0]
        
        #tag[0] is the word, tag[1] is the actual POS-tag:
        if (tag[1] in ['NN', 'NNS' , 'NNP', 'NNPS']): #nouns
            NN.add(tag[0])
        elif (tag[1] in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']): #verbs
            VB.add(tag[0])
        elif (tag[1] in ['JJ', 'JJR', 'JJS']): #adjectives
            ADJ.add(tag[0])
        elif (tag[1] in ['RB', 'RBR', 'RBS', 'WRB']): #adverbs
            ADV.add(tag[0])
        elif (tag[1] in ['CD']): #cardinal digit
            NUM.add(tag[0])
    
    #Not returning ADJ, ADV, NUM so far as I've no clue what to do with them
    return(NN, VB)
 
def text_set_creation_lemmatized(ents : list, lemmatizer = WordNetLemmatizer) -> list:
    '''For a list of tokens, POS-tag all of them
    Returns a list of tuples (word, POS-tag)'''

    #Sets storing NouNs, VerBs, ADJectives, ADVerbs and NUMerals respectively:
    NN, VB, ADJ, ADV, NUM = set(()), set(()), set(()), set(()), set(())

    #Iterating over all the tokens in this given section:
    for word in ents:
    
        #tag[0] is the word, tag[1] is the actual POS-tag:
        tag = nltk.pos_tag([word])[0] #has to be converted to a list to tag
    
        if (tag[1] in ['NN', 'NNS' , 'NNP', 'NNPS']): #nouns
            word = lemmatizer.lemmatize(tag[0], 'n')
            NN.add(word)
        elif (tag[1] in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']): #verbs
            word = lemmatizer.lemmatize(tag[0], 'v')
            VB.add(word)
        elif (tag[1] in ['JJ', 'JJR', 'JJS']): #adjectives
            word = lemmatizer.lemmatize(tag[0], 'a')
            ADJ.add(word)
        elif (tag[1] in ['RB', 'RBR', 'RBS', 'WRB']): #adverbs
            word = lemmatizer.lemmatize(tag[0], 'r')
            ADV.add(word)
        elif (tag[1] in ['CD']): #cardinal digit
            NUM.add(tag[0])
    
    #Not returning ADJ, ADV, NUM so far as I've no clue what to do with them
    return(NN, VB)
    
def finding_max_similarity(set1 : set, set2 : set, nouns : bool, corpus=brown_ic) -> tuple:
    '''also deletes the proper words from the two sets
    returns: (word1, word2, score)'''
    
    
    if (len(set1) == 0 or len(set2) == 0):
        return("Dude, you can't use finding_max_similarity on empty sets!")
    
    #We can do this, since we only will be dealing with non-negative values 
    #(and we don't need to know when does the maximum equal 0)
    maximum = -1.0
    maximum_word = ''
    
    #Choosing the proper suffix for the library - based on whether the word is a noun or a verb
    suffix = ['.n.01', '.v.01']
    if (nouns):
        suffix = suffix[0]
    else:
        suffix = suffix[1]
    
    #Iterating over the first set, but actually only a single element of it (hence the break later)
    for element1 in set1:
        
        #converting the word to an object usable by WordNet:
        try:
            element1_object = wn.synset(element1 + suffix)
        except: #If the word is incomprehensible (e.g. last name), terminate the function:
            set1.remove(element1)
            #print(element1)
            return('', '', -1.0)
        
        #Iterating over all the elements of set2:
        for element2 in set2:
            
            #Converting the word to an object usable by WordNet:
            try:
                element2_object = wn.synset(element2 + suffix)
                
                #Calculating the Lin similarity score:
                score = element1_object.lin_similarity(element2_object, corpus)
                
                #If Lin score returns -0.0, use the Wu-Palmer score:
                #if (score == 0.0):
                #    score = element1_object.wup_similarity(element2_object)
            except:
                score = -1
            
            #If a new maximum found, update maximum and maximum_word:
            if (score >= maximum):
                maximum = score
                maximum_word = element2
            
        break #breaking because we only care about a single word in this function
    
    #Remove the word from the first set:
    set1.remove(element1)
    
    #If any matching word has been found in set2, remove it as well:
    if (maximum_word != ''):
        set2.remove(maximum_word)
        
    #The removals are indeed in-place, hence no need to return the set objects! 
    return(element1, maximum_word, maximum)
    
def finding_sum_of_max_similarities(set1 : set, set2 : set, nouns : bool, df : pd.DataFrame, corpus=brown_ic, 
                                    threshold = 0.02, verbose = False):
    '''
    set1:           the first set with words
    set2:           the 2nd set with words
    nouns:          True means we are inspecting nouns, False means we are inspecting verbs
    df:             the DataFrame with IDFs
    corpus:         pass brown_ic (used in the Lin similarity score)
    threshold:      not including any similarity values falling below or equal this threshold
    '''
    
    #Setting up the sum counters:
    sum_products = 0.0
    #number_similarities = 0
    sum_idfs = 0.0
    
    if (verbose):
        print("set 1 cardinality:", len(set1), "set 2 cardinality:", len(set2))
    
    #Only performing this matching if none of the sets is empty:
    if(len(set1) == 0 or len(set2) == 0):
        return(0) #If at least one is empty, immediately reutrn 0
    
    #Running finding_max_similarity for as long as there exist words in both sets (each iteration deletes 1 word):
    while(len(set1) > 0 and len(set2) > 0):
        (word1, word2, score) = finding_max_similarity(set1, set2, nouns, corpus=corpus)
        
        #Only interested in results exceeding the threshold:
        if (score > threshold):
            
            #Updating the sum counters:
            idf = float(df[word1])
            sum_idfs += idf
            #number_similarities += 1
            if (verbose):
                print(score*idf)
            sum_products += score*idf
    if (verbose):
        print("there are", len(set1), " items left in set1,", len(set2), " items left in set2.")
    
    #If there were any matches at all:
    if(sum_idfs > 0):
        #Total score is sum[maxSim(word) * idf(word)] / sum[idf(word)]
        total_score = sum_products / sum_idfs
        
    else: #If there are no matches:
        total_score = 0.0
        
    return(total_score)


#score_for_2_texts notebook:
def fix_publication_date(text : str):
    return(str(text)[:10])
    
def fix_dataframe(df : pd.DataFrame):
    '''
    ?
    '''
    
    length = round(df.shape[0]/2)
    df = df.drop([2*x for x in range(0, length)], axis=0)
    df = df.reset_index(drop = True)
    df['acl_pdf_link'] = [0] * df.shape[0]
    df['acl_publication_date'] = [0] * df.shape[0]
    df['score'] = [-2] * df.shape[0]
    df['publication_date'] = df['publication_date'].apply(fix_publication_date)
    
    return(df)

def process_two_string_papers(doc1 : str, doc2 : str, verbose = False, nlp=nlp, corpus = brown_ic) -> float:
    '''
    Missing the proper sectionization so far
    '''
    
    #Cutting off the irrelevant parts of the papers:
    doc1, doc2 = cut_away_irrelevant(doc1, verbose=False), cut_away_irrelevant(doc2, verbose=False)
    
    #SECTIONIZE!
    #SO FAR WE'LL PROCEED WITHOUT THE PROPER SECTIONIZATION
    
    #Tokenization of both papers:
    if (verbose):
        print("Tokenizing the first paper")
    ents1 = tokenization_of_section(doc1)
    
    if(verbose):
        print("Tokenizing the second paper")
    ents2 = tokenization_of_section(doc2)
    
    #Creating the IDF matrices:
    if (verbose):
        print("Creating the first IDF matrix")
    df1 = creating_idf_matrix(ents1)
    
    if (verbose):
        print("Creating the second IDF matrix")
    df2 = creating_idf_matrix(ents2)
    
    #Converting the first and second text into sets:
    if (verbose):
        print("Converting both token lists into sets")
    NN1, VB1 = text_set_creation(ents1)
    NN2, VB2 = text_set_creation(ents2)
    
    #Calculating the nouns and verbs scores for similarity(D1, D2):
    if (verbose):
        print("Finding the final score for nouns:") 
    score1_nouns = finding_sum_of_max_similarities(set1=NN1.copy(), set2=NN2.copy(), nouns=True, df=df1, 
                                                       threshold = 0.1, verbose=verbose, corpus=corpus)
    if (verbose):
        print("Finding the final score for verbs:") 
    score1_verbs = finding_sum_of_max_similarities(set1=VB1.copy(), set2=VB2.copy(), nouns=False, df=df1, 
                                                       threshold = 0.1, verbose = verbose, corpus=corpus)
    
    #Calculating the nouns and verbs scores for similarity(D2, D1):
    if (verbose):
        print("Finding the final score for nouns:") 
    score2_nouns = finding_sum_of_max_similarities(set1=NN2, set2=NN1, nouns=True, df=df2, 
                                                       threshold = 0.1, verbose=verbose, corpus=corpus)
    if (verbose):
        print("Finding the final score for verbs:") 
    score2_verbs = finding_sum_of_max_similarities(set1=VB2, set2=VB1, nouns=False, df=df2, 
                                                       threshold = 0.1, verbose = verbose, corpus=corpus)
    
    #Averaging out the scores:
    score_final_nouns, score_final_verbs = (score1_nouns+score2_nouns)/2, (score1_verbs+score2_verbs)/2
    
    return(score_final_nouns, score_final_verbs)

def update_one_df_row(df : pd.DataFrame, index : int, driver, main_tab : str, verbose=False, 
                        delete_after_download=True, nlp=nlp, corpus = brown_ic) -> None:
    '''
    If the papers could not be downloaded, putting -1 into the database
    '''
    
    #Downloads the Arxiv version:
    driver.get(df.loc[index, 'pdf_link'])
    time.sleep(9)
    try:
        os.rename(os.listdir()[0], "000_arx.pdf")
    except PermissionError:
        time.sleep(10)
        os.rename(os.listdir()[0], "000_arx.pdf")
        
        
        
        
        
        
    doc1 = pdf_reader('000_arx.pdf', nlp)
    doc1 = str(doc1)
    os.remove(os.listdir()[0])
    return(len(doc1))
    
    
    
    
    
    
    #Downloads the ACL version:
    driver.get(df.loc[index, 'acl_pdf_link'])
    time.sleep(9)
    if (os.listdir()[0] == "000_arx.pdf"):
        try:
            os.rename(os.listdir()[1], "000_acl.pdf")
        except PermissionError:
            time.sleep(15)
            os.rename(os.listdir()[1], "000_acl.pdf")
    else:
        try:
            os.rename(os.listdir()[0], "000_acl.pdf")
        except PermissionError:
            time.sleep(15)
            os.rename(os.listdir()[0], "000_acl.pdf")
    
    #Read the PDFs:
    if (verbose):
        print("Reading the PDFs of the papers")
    doc1 = pdf_reader('000_arx.pdf', nlp)
    doc2 = pdf_reader('000_acl.pdf', nlp)
    
    #Convert the PDFs to strings:
    if (verbose):
        print("Converting the PDFs to strings")
    doc1, doc2 = str(doc1), str(doc2)
    
    #Deleting the PDFs from the location:
    if (delete_after_download):
        os.remove(os.listdir()[0])
        os.remove(os.listdir()[0])
    
    #Calculating the similarity score;
    score_final_nouns, score_final_verbs = process_two_string_papers(doc1, doc2, verbose=verbose, 
                                                                     nlp=nlp, corpus=corpus)
    score_final = (score_final_nouns + score_final_verbs) / 2
    
    #Saving the score into the database
    #We don't have to return the df again, as the function mutates it
    df.loc[index, 'score'] = score_final
    
    return(score_final) #perhaps change to None in production code?


#Database iteration notebook:
def database_iteration(df : pd.DataFrame, index_start : int, index_end : int, verbose=False) -> str:
    '''
    Plug in the dataframe wth only non-zero rows
    Downloads the two papers and performs the analysis on them, saves the info into the df
    Also automatically updates the df
    Only returns to "output2.csv", then rename this file accordingly
    '''
    
    os.chdir(bep.download_dir)
    driver, main_tab = create_driver()
    time.sleep(2)
    
    for i in range(index_start, index_end):
        if(verbose):
            print(i)
        
        #Only enter the row if it hasn't been tried yet:
        #if (df.loc[i, 'score'] == -2 or df.loc[i, 'score'] == -1):
        if (df.loc[i, 'score'] == -2):
            try:
                bep.update_one_df_row(df, index=i, driver=driver, main_tab=main_tab, verbose=verbose, 
                                  delete_after_download=True, nlp=bep.nlp, corpus=bep.brown_ic)
            except IndexError: #If for some reason the pdf couldn't 
                df.loc[i, 'score'] = -1
            #print("wow")
            time.sleep(3)
            
    #Overwriting the database with the updated one:
    df.to_csv("C:\\Users\\user\\Desktop\\TuE\\BEP\\output2.csv", index=False, sep = ";")
    
    driver.quit()
    
    print("finished!")
    return("Finished")

def filling_in_acl_info(df, index_start : int, index_end : int) -> None:
    '''
    Uses the driver to try to extract the ACL download link (and the date) for any arxiv paper in the dataframe
    '''
    
    #Setting up the driver:
    driver, main_tab = bep.create_driver()
    time.sleep(2)
    
    #Iterating over the rows of the dataframe:
    for i in range(index_start, index_end):
        
        #Only if the specific cells haven't yet been updated (correctly or incorrectly):
        if ((df['acl_pdf_link'][i] == 0 and df['acl_publication_date'][i] == 0)
           or (df['acl_pdf_link'][i] == '0' and df['acl_publication_date'][i] == '0')):
            
            #Extract the title from the dataframe:
            title = df['title'][i]
            
            #trying to extract the acl_pdf_link and the acl_publication_date:
            output = bep.save_pdf_link_and_date(driver=driver, main_tab=main_tab, title=title, 
                                            month_name=bep.month_name, year_name=bep.year_name)
            
            #If the output is not None:
            if (output != None):
                df['acl_pdf_link'][i] = output[0]
                df['acl_publication_date'][i] = str(output[1]) + "-" + str(output[2])
                
            time.sleep(1)

def acl_text_to_datetime(text : str):
    '''Always assuming the day is 31th...so that we do not need to drop any more data'''
    month = text[:-5] #assuming a year always has 4 digits...sorry, future people!
    year = text[-4:]
    date_text = '15 ' + month + ' ' + year
    datetime_object = datetime.datetime.strptime(date_text, '%d %B %Y')
    return(datetime_object)

def arxiv_text_to_datetime(text : str):
    return(datetime.datetime.strptime(text, '%d.%m.%Y'))

def calculate_time_interval(df : pd.DataFrame):
    '''uses the publication_date and acl_publication_date columns
    For acl dates, assumes the 15th day of the month always
    Yes, it is slow, but the fast versions of these functions were errorenous (due to my misspelling)'''

    if ('time_interval' not in df.columns):
        df['time_interval'] = [0] * df.shape[0]
        for i in range(0, df.shape[0]):
            df.loc[i, 'time_interval'] = (acl_text_to_datetime(df.loc[i, 'acl_publication_date']) - arxiv_text_to_datetime(df.loc[i, 'publication_date'])).days
        
    return(df)


#Potentially useful functions (but perhaps no):
def use_acl_scraper(titles : list, download_dir : str, verbose = False):

    '''The old version, where chaining the downloads was the way to go'''
    
    i = 0
    driver, main_tab = create_driver(download_dir = download_dir)
    
    for title in titles:
        i = i+1
        download_paper(title, driver, main_tab, verbose=verbose, save_as_title = True)
        print("Downloaded paper {} out of {}".format(i, len(titles)))
        
    return("Finished")

def download_one_paper(df : pd.DataFrame, index : int, driver, main_tab, download_dir = download_dir, 
                       verbose=False, delete_after_download=True):
    '''In future, declare the driver just once for better performance
    Always use delete_after_download in production code'''
    
    os.chdir(download_dir)
    title = df.loc[index, 'title']
    
    #Downloads the paper from ACL:
    #driver, main_tab = create_driver()
    download_paper(title=title, driver=driver, main_tab=main_tab, verbose = False, save_as_title = False)
    
    #So that the process can terminate:
    time.sleep(5)
    
    #Checks if the file got downloaded and renames it:
    if(len(os.listdir()) == 1):
        os.rename(os.listdir()[0], "00_acl.pdf")
    else:
        print("The ACL PDF could not be found!")
        return(None)
    
    #Downloads the paper from ArXiv:
    #Forcing the download of the 1st version
    pdf_link = df.loc[index, 'pdf_link'][:-2] + 'v1'
    driver.get(pdf_link)
    time.sleep(10) #long sleep required here
    
    #Checks if the file got downloaded and renames it:
    if(len(os.listdir()) == 2):
        os.rename(os.listdir()[1], "00_arxiv.pdf")
    else:
        print("The arxiv PDF could not be found!")
        os.remove(os.listdir()[0])
        return(None)
    
    #Switching to the tab to be closed:
    driver = switch_tabs(driver, main_tab)
    driver.close() #Closing
    driver.switch_to.window(main_tab) #untested, switches to the main tab
      
    #This will trigger only if both PDFs were found (so return did not get triggered)
    #Read the PDFs:
    if (verbose):
        print("Reading the PDFs of the papers")
    doc1 = pdf_reader('00_arxiv.pdf', nlp)
    doc2 = pdf_reader('00_acl.pdf', nlp)
    
    #Cenvert the PDFs to strings:
    if (verbose):
        print("Converting the PDFs to strings")
    doc1, doc2 = str(doc1), str(doc2)
    
    #Deleting the PDFs from the location:
    if (delete_after_download):
        os.remove(os.listdir()[0])
        os.remove(os.listdir()[0])
    
    return(doc1, doc2)

#That's it!